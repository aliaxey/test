package com.example.akaraban.test;

import android.databinding.ObservableField;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.example.akaraban.test.adapters.FieldsRecyclerViewAdapter;
import com.example.akaraban.test.models.Field;
import com.example.akaraban.test.models.FieldsList;
import com.example.akaraban.test.ui.MainActivity;
import com.example.akaraban.test.ui.ProgressDiallg;
import com.example.akaraban.test.ui.ResultDialog;
import com.google.gson.JsonObject;
import com.stfalcon.androidmvvmhelper.mvvm.activities.ActivityViewModel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MainActivityVM extends ActivityViewModel<MainActivity> {

    //public final ObservableBoolean inProcess = new ObservableBoolean(true);
    public final ObservableField<String> url = new ObservableField<>();
    List<Field> list;
    @BindView(R.id.form)
    RecyclerView recyclerView;
    static FieldsRecyclerViewAdapter adapter = null;




    public MainActivityVM(MainActivity activity) {
        super(activity);
        ButterKnife.bind(this,activity);
        Call<FieldsList> call = Utils.getInput().getFieldList();;
        DialogFragment dialog = new ProgressDiallg();
        ((ProgressDiallg) dialog).setText(R.string.load_form);
        ((ProgressDiallg) dialog).setStop(()->call.cancel());
        call.enqueue(new Callback<FieldsList>() {
            @Override
            public void onResponse(Call<FieldsList> call, Response<FieldsList> response) {
                if(response.body()!=null){
                    setTitle(response.body().getTitle());
                    url.set(response.body().getImage());
                    list = response.body().getFields();
                    setAdapter();
                    dialog.dismiss();
                }else {
                    onFailure(call, new NullPointerException("No body"));
                }
            }

            @Override
            public void onFailure(Call<FieldsList> call, Throwable t) {
                dialog.dismiss();
                Toast.makeText(activity,t.toString(),Toast.LENGTH_LONG).show();
            }
        });
        dialog.show(getActivity().getSupportFragmentManager(),"load");


    }


    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onStart() {
        super.onStart();
    }

    private  void onSubmit(){
        DialogFragment dialog = new ProgressDiallg();
        ((ProgressDiallg) dialog).setText(R.string.load_answer);
        dialog.show(getActivity().getSupportFragmentManager(),"upload");
        JsonObject result = ((FieldsRecyclerViewAdapter)recyclerView.getAdapter()).getResult();
        Call<com.example.akaraban.test.models.Response> call = Utils.getInput().submitResults(result);
        ((ProgressDiallg) dialog).setStop(()->call.cancel());
        call.enqueue(new Callback<com.example.akaraban.test.models.Response>() {
            @Override
            public void onResponse(Call<com.example.akaraban.test.models.Response> call, Response<com.example.akaraban.test.models.Response> response) {
                dialog.dismiss();
                if(response != null){
                    DialogFragment resultDialog = new ResultDialog();
                    ((ResultDialog) resultDialog).setResult(response.body().getResult());
                    resultDialog.show(activity.getSupportFragmentManager(),"result");
                }
            }

            @Override
            public void onFailure(Call<com.example.akaraban.test.models.Response> call, Throwable t) {
                dialog.dismiss();
                Toast.makeText(activity,t.toString(),Toast.LENGTH_LONG).show();
            }
        });
    }
    private void setAdapter(){
        addSendButton();
        if(adapter == null){
            adapter = new FieldsRecyclerViewAdapter(activity,list);
        }
        adapter.setOnSubmitListener((v)-> onSubmit());
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(activity));
    }
    private void addSendButton(){
        Field field = new Field();
        field.setType("SUBMIT");
        list.add(field);
    }
    private void setTitle(String title){
        activity.getSupportActionBar().setTitle(title);
    }
}
