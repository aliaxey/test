package com.example.akaraban.test;

import android.app.Application;

import com.example.akaraban.test.interfaces.InputAPI;

import retrofit2.*;
import retrofit2.converter.gson.GsonConverterFactory;

public class Utils extends Application {
    static final String BASE_URL = "http://test.clevertec.ru";

    private static InputAPI input;
    private static Retrofit retrofit;

    @Override
    public void onCreate() {
        retrofit = new Retrofit.Builder().baseUrl(BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        input = retrofit.create(InputAPI.class);
        super.onCreate();
    }

    public static InputAPI getInput() {
        return input;
    }

}
