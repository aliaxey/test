package com.example.akaraban.test.adapters;

import android.databinding.BindingAdapter;
import android.databinding.BindingConversion;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

public final class DataBindingAdapters {
    @BindingAdapter("android:src")
    public static void loadImage(ImageView view, String url){
        Picasso.with(view.getContext()).load(url).into(view);
    }
    @BindingConversion
    public static int isVisible(boolean isVisible){
        return isVisible?View.VISIBLE:View.INVISIBLE;
    }
    /*@BindingAdapter({"android:src", "app:error"})
    public static void loadImage(ImageView view, String url, Drawable error) {
        Picasso.with(view.getContext())
                .load(url)
                .error(error)
                .into(view);
    }*/
}
