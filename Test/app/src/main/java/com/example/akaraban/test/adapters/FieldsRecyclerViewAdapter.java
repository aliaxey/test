package com.example.akaraban.test.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.example.akaraban.test.R;
import com.example.akaraban.test.models.Field;
import com.google.gson.JsonObject;

import java.util.List;

public class FieldsRecyclerViewAdapter extends RecyclerView.Adapter<FieldsViewHolder> {
    static final String TEXT_TYPE = "TEXT"; //0
    static final String NUMBER_TYPE = "NUMERIC"; //1
    static final String LIST_TYPE = "LIST"; //2
    static final String SUBMIT_TYPE = "SUBMIT"; //3
    static List<Field> fields;
    Context context;
    View.OnClickListener onSubmitListener;
    JsonObject result;

    public void setOnSubmitListener(View.OnClickListener onSubmitListener) {
        this.onSubmitListener = onSubmitListener;
    }

    public FieldsRecyclerViewAdapter(Context context, List<Field> fields) {
        this.fields = fields;
        this.context = context;
    }

    @Override
    public int getItemViewType(int position) {
        switch (fields.get(position).getType()){
            case TEXT_TYPE:
                return 0;
            case NUMBER_TYPE:
                return 1;
            case LIST_TYPE:
                return 2;
            case SUBMIT_TYPE:
                return 3;
                default:
                    return -1;
        }
    }

    @NonNull
    @Override
    public FieldsViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(context);
        switch(i){
            case 0:
                return new TextViewHolder(inflater.inflate(R.layout.text_item_field,viewGroup,false));
            case 1:
                return new NumericViewHolder(inflater.inflate(R.layout.number_item_field,viewGroup,false));
            case 2:
                return new ListViewHolder(inflater.inflate(R.layout.list_item_field,viewGroup,false));
            case 3:
                return new SubmitViewHolder(inflater.inflate(R.layout.submit_item,viewGroup,false));
                default:
                    return null;
        }
    }
    @Override
    public void onBindViewHolder(@NonNull FieldsViewHolder fieldsViewHolder, int i) {
        Field item = fields.get(i);
        fieldsViewHolder.getName().setText(item.getTitle());
        switch (item.getType()) {
            case TEXT_TYPE:
                if(item.getStringValue()!= null){
                    ((TextViewHolder)fieldsViewHolder).getField().setText(item.getStringValue());
                }
                ((TextViewHolder)fieldsViewHolder).getField().addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        item.setStringValue(((TextViewHolder)fieldsViewHolder).getField().getText().toString());
                    }

                    @Override
                    public void afterTextChanged(Editable s) {

                    }
                });
                break;
            case NUMBER_TYPE:
                if(item.getStringValue()!= null){
                    ((NumericViewHolder)fieldsViewHolder).getField().setText(item.getStringValue());
                }
                ((NumericViewHolder)fieldsViewHolder).getField().addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        item.setStringValue(s.toString());
                    }

                    @Override
                    public void afterTextChanged(Editable s) {

                    }
                });

                break;
            case LIST_TYPE:
                Spinner spinner = ((ListViewHolder) fieldsViewHolder).getField();
                List<String> stringList = item.getValues().asList();
                SpinnerAdapter spinnerAdapter = new ArrayAdapter<>(context, android.R.layout.simple_dropdown_item_1line, stringList);
                spinner.setAdapter(spinnerAdapter);
                spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        switch (position){
                            case 0:
                                item.setStringValue("none");
                                break;
                            case 1:
                                item.setStringValue("v1");
                                break;
                            case 2:
                                item.setStringValue("v2");
                                break;
                            case  3:
                                item.setStringValue("v3");
                                break;
                            default:
                                item.setStringValue("none");
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
                break;
            case SUBMIT_TYPE:
                ((SubmitViewHolder) fieldsViewHolder).getSubmit().setOnClickListener((v) ->sendResult(v));
                break;
        }

    }



    @Override
    public int getItemCount() {
        return fields.size();
    }

    public JsonObject getResult() {
        return result;
    }
@Deprecated
    private boolean fieldsNotNull(){
        for (Field field:fields){
            if(field.getStringValue()== null){
                Toast.makeText(context,R.string.err_null_fields,Toast.LENGTH_LONG).show();
                return false;
            }
        }
        return true;
    }

    private void sendResult(View v){
        result = new JsonObject();
        JsonObject form = new JsonObject();
        for(Field field:fields){
            if(field.getStringValue()== null&&!field.getType().equals(SUBMIT_TYPE)) {
                Toast.makeText(context, R.string.err_null_fields, Toast.LENGTH_LONG).show();
                return;
            }
            switch (field.getType()){
                case NUMBER_TYPE:
                    if(!field.getStringValue().isEmpty()) {
                        form.addProperty(field.getName(), Double.parseDouble(field.getStringValue()));
                    }else {
                        form.addProperty(field.getName(),0);
                    }
                    break;
                case TEXT_TYPE:
                case LIST_TYPE:
                    form.addProperty(field.getName(),field.getStringValue());
                    break;

            }
        }
        result.add("form",form);
        onSubmitListener.onClick(v);
    }
}