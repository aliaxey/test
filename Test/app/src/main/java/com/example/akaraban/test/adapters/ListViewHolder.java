package com.example.akaraban.test.adapters;

import android.support.annotation.NonNull;
import android.view.View;
import android.widget.Spinner;

import com.example.akaraban.test.R;

public class ListViewHolder extends FieldsViewHolder {
    Spinner field;
    public ListViewHolder(@NonNull View itemView) {
        super(itemView);
        field = (Spinner)itemView.findViewById(R.id.spinner);
    }

    public Spinner getField() {
        return field;
    }

    public void setField(Spinner field) {
        this.field = field;
    }
}
