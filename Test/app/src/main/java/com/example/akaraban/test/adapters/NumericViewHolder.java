package com.example.akaraban.test.adapters;

import android.support.annotation.NonNull;
import android.view.View;
import android.widget.EditText;

import com.example.akaraban.test.R;

public class NumericViewHolder extends FieldsViewHolder {
    EditText field;
    public NumericViewHolder(@NonNull View itemView) {
        super(itemView);
        field = (EditText)itemView.findViewById(R.id.field);
    }

    public EditText getField() {
        return field;
    }

    public void setField(EditText field) {
        this.field = field;
    }
}
