package com.example.akaraban.test.adapters;

import android.support.annotation.NonNull;
import android.view.View;
import android.widget.Button;

import com.example.akaraban.test.R;

public class SubmitViewHolder extends FieldsViewHolder {
    private Button submit;
    public SubmitViewHolder(@NonNull View itemView) {
        super(itemView);
        submit = (Button)itemView.findViewById(R.id.submit);
    }

    public Button getSubmit() {
        return submit;
    }

    public void setSubmit(Button submit) {
        this.submit = submit;
    }
}
