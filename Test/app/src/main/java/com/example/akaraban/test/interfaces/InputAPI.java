package com.example.akaraban.test.interfaces;

import com.example.akaraban.test.models.FieldsList;
import com.example.akaraban.test.models.Response;
import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface InputAPI {
    @GET("/tt/meta")
    Call<FieldsList> getFieldList();
    @POST("/tt/data")
    Call<Response> submitResults(@Body JsonObject form);
}
