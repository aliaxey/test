
package com.example.akaraban.test.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Values {

    @SerializedName("none")
    @Expose
    private String none;
    @SerializedName("v1")
    @Expose
    private String v1;
    @SerializedName("v2")
    @Expose
    private String v2;
    @SerializedName("v3")
    @Expose
    private String v3;

    public String getNone() {
        return none;
    }

    public void setNone(String none) {
        this.none = none;
    }

    public String getV1() {
        return v1;
    }

    public void setV1(String v1) {
        this.v1 = v1;
    }

    public String getV2() {
        return v2;
    }

    public void setV2(String v2) {
        this.v2 = v2;
    }

    public String getV3() {
        return v3;
    }

    public void setV3(String v3) {
        this.v3 = v3;
    }

    public List<String> asList(){
        List<String> list = new ArrayList<>();
        list.add(none);
        list.add(v1);
        list.add(v2);
        list.add(v3);
        return list;
    }

}
