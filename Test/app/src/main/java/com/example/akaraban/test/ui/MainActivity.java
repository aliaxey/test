package com.example.akaraban.test.ui;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.example.akaraban.test.BR;
import com.example.akaraban.test.MainActivityVM;
import com.example.akaraban.test.R;
import com.example.akaraban.test.databinding.ActivityMainBinding;
import com.stfalcon.androidmvvmhelper.mvvm.activities.BindingActivity;

public class MainActivity extends BindingActivity<ActivityMainBinding,MainActivityVM> {

    @Override
    public MainActivityVM onCreate() {
        return new MainActivityVM(this);
    }

    @Override
    public int getVariable() {
        return BR.viewmodel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_main;
    }
}
