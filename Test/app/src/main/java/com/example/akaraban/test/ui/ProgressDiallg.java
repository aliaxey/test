package com.example.akaraban.test.ui;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.akaraban.test.R;

public class ProgressDiallg extends DialogFragment {
    int text;
    TextView message;
    OnCancel stop;

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.load);
        builder.setNegativeButton(R.string.cancel, (d,i)-> {
            stop.onCancel();
            d.dismiss();
        });
        builder.setView(R.layout.progress);
        AlertDialog dialog = builder.show();
        message = dialog.findViewById(R.id.message);
        message.setText(text);
        return dialog;

    }
    public void setText(int text){
        this.text = text;
    }

    public void setStop(OnCancel stop) {
        this.stop = stop;
    }

    public interface OnCancel{
        void onCancel();
    }
}
