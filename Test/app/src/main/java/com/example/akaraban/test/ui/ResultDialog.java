package com.example.akaraban.test.ui;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;

import com.example.akaraban.test.R;

public class ResultDialog extends DialogFragment {
    String result;
    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.success);
        builder.setMessage(result);
        builder.setPositiveButton(R.string.ok,(d,id)->d.dismiss());
        return builder.create();
    }

    public void setResult(String result) {
        this.result = result;
    }
}
